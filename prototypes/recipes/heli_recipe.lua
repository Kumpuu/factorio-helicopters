data:extend({
	{
		type = "recipe",
		name = "heli-recipe",
		enabled = false,
		ingredients = {
			{"engine-unit", 40},
			{"steel-plate", 30},
			{"iron-gear-wheel", 50},
			{"advanced-circuit", 50},
			{"plastic-bar", 40},
		},
		result = "heli-item",
	}
})