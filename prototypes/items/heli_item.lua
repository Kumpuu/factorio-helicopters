data:extend({
	{
		type = "item-with-entity-data",
		name = "heli-item",
		icon = "__Helicopters__/graphics/icons/heli.png",
		flags = {"goes-to-quickbar"},
		subgroup = "transport",
		order = "b[personal-transport]-c[heli]",
		place_result = "heli-placement-entity-_-",
		stack_size = 1
	}
})